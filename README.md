# THIS PIECE OF SOFTWARE IS DEPRECATED

because it's completely useless. Please, use something else

---

Generates passwords consisting of random Unicode characters from the whole table (1114111 characters)

## How to install

In the browser:

```
<script src="Assworder.js"></script>
```

In NodeJS:

```
var Assword = require("assworder");
```

## How to use

In the script:

```
new Assword(256) //Returns a 256 character long assword
new Assword(256, "String") //Returns an assword truncated to 256 JavaScript characters (e.g. "a" length is 1 but "🔥" length is 2)
new Assword(256, "Byte") //Returns an assword truncated to 256 bytes (e.g. "w" is 1 byte but "ы" is 2)
```

In the console:

```
assworder 256
assworder 256 String
assworder 256 Byte
```
